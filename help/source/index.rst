ST_LUCAS QGIS Plugin
====================

.. image:: images/st_lucas_architecture.png
   :width: 600px

The plugin allows to interactively select the spatial, attribute,
temporal and thematic filters from the GUI. In particular, it allows
selecting an area of interest (spatial filter) by the extent of the
map canvas, specifying a country from a list of EU countries or using
a user-defined vector polygon data layer. The plugin interface allows
specifying also a list of selected years (temporal filter) and a group
of attributes (thematic filter). The plugin also integrates
functionality for performing a user-defined class aggregation using a
JSON file, a nomenclature translation using a CSV file and showing
photos of a selected LUCAS point.

The plugin consists of three tabs:

.. toctree::
   :maxdepth: 2
              
   download
   analyze
   photos
    
Known issues
------------

OWSLib library contains in versions 0.23-0.25 a critical
bug, which affects this plugin. Please upgrade your OWSLib to version
0.26. Go to `Plugins -> Python console` and type:

.. code-block:: python

   import pip
   pip.main(["install", "owslib>=0.26"])

and restart QGIS.
