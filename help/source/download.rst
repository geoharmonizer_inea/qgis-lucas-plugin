Download
--------

The first tab is used to download LUCAS points, where the user has to set individual parameters for downloading.

.. image:: images/download_tab.png

Area of interest
****************

In the first part, the user sets the area of interest. There are three options to choose from. If the user selects the ``canvas`` option, only LUCAS points that fall within the map window are downloaded.

.. image:: images/aoi_canvas.png

If the user selects the ``country`` option, a combo box will be available to select the countries for which LUCAS points will be downloaded. More than one country may be selected.

.. image:: images/aoi_country.png

If the user selects the ``vector layer`` option, the plugin will allow the user to select a polygon layer loaded into QGIS as the area of interest for downloading LUCAS points. The CRS of choosen polygon layer must be **EPSG:3035!** Otherwise, the download will end with an error.

.. image:: images/aoi_vector.png

Years
*****

The second part allows the user to select the years in which LUCAS points were measured. There are five years available: 2006, 2009, 2012, 2015 and 2018. More than one year may be selected. The user can also select the ``all`` option, which will return all years.

.. image:: images/years.png

.. note::

   Not all countries are available for all years. For example the year 2006 contains only 11 contries.

Groups of attributes
********************

In the third part, the user can choose which attributes in addition to the basic attributes will be included in the attribute table of downloaded LUCAS points. The user can choose ``all`` option, which returns all available attributes or one of the available groups of attributes can be selected. The following groups are available: Land Cover, Land Use; Land Cover, Land Use, Soil; Forestry; Copernicus; Inspire PLCC.

.. image:: images/groups.png

Space-time aggregation
**********************

If more than one year has been selected, the user can decide whether to use space-temporal aggregation or not.

.. image:: images/st_aggregation.png

Output GeoPackage
*****************

The last thing the user has to choose is where the geopackage with LUCAS points will be stored via tool button ``...``. After this the LUCAS points can be downloaded by the ``Download`` button.

.. image:: images/output.png

If the download was successful, information about the number of downloaded LUCAS points will be displayed as push message.

.. image:: images/download.png

LUCAS points with predefined style corresponding with level 1 LUCAS nomenclature will be displayed in map window.

.. image:: images/lucas_points.png

Level 1 nomenclature:

.. image:: images/legend.png

.. note::

   LUCAS points with circle symbol indicate, that there are available photos of the LUCAS point. Square symbol means, that there are no photos available.

.. note::

   In some cases it may be that there is no photograph of the point itself, but photographs of the cardinal directions are available. In such a situation, the point still has a square symbol, but the world direction photos can be displayed. For 2006, all points have a square symbol because it is not possible to determine from the attributes whether photos are available for a given point. (The necessary attributes have not yet been introduced for 2006.) However, this does not mean that the plugin is unable to display the available photos.

.. note::

   If Space-time aggregation is checked, no QML style is currently available. This is due to the limit of the current implementation. The solution to the problem is intended for further development.
