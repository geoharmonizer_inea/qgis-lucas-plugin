Photos
------

The third tab is used for showing the photos of selected LUCAS point.

.. image:: images/photo_tab.png

After selecting a LUCAS point, the photo can be shown by changing the ``Choose photo`` combo box to one of the five options: **Point**, **North**, **South**, **West** or **East**. Along with the photo, information about the land cover class of the LUCAS point, LUCAS point ID and year of measurement are displayed.

.. note::

   Only one point can be selected. In other cases, the plugin returns a warning message.

.. note::

   Showing photos for space-time aggregated data is not implemented yet. This is due to the limit of the current implementation. The solution to the problem is intended for further development.

.. image:: images/lucas_photo.png

The size of the loaded photos is adjusted to the size of the plugin window. To view a photo in a larger size, enlarge the plugin window and click the ``Refresh`` button.

.. image:: images/change_size.png

In most cases, LUCAS point has five photos. To display individual photos, either change the value in the combo box or use the ``Previous`` and ``Next`` buttons.

.. note::

   Loading photos into the plugin may take some time.

.. image:: images/prev_and_next.png

To show photo in full resolution there is the ``Show Photo in Web Browser`` button which will open the url adress of the given photo. There is also ``Clear Photo`` button which clears the photo if needed.

In addition, all five photos can be downloaded as a zip archive using the ``Download All`` button.
